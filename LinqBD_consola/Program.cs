﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD_consola
{
    class Program
    {
        static void Main2(string[] args)
        {
            // vamos instanciar o DC que é o objeto que vamos utilizar para trabalhar com a base de dados
            DataClasses1DataContext dc = new DataClasses1DataContext();

            // Obter a listagem completa de filmes
            // vai à tabela filmes procurar objetos do tipo filme, ordernar por titulo e selecionar como filme
            var lista = from Filme in dc.Filmes orderby Filme.titulo select Filme;

            //
            var outralista = from Filme in dc.Filmes orderby Filme.titulo descending, Filme.id select Filme;

            foreach (Filme filme in lista)
            {
                Console.WriteLine("ID: " + filme.id);
                Console.WriteLine("Titulo: " + filme.titulo);
                Console.WriteLine("Categoria: " + filme.categoria);
                Console.WriteLine();
            }

            Console.WriteLine("Existem {0} filmes.", lista.Count());

            Console.ReadKey();
        }
    }
}
