﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD_consola
{
    class ManipulaDados
    {
        static void Main(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();

            //Inserir novo registo
            Filme f = new Filme
            {
                id = dc.Filmes.Count() + 100,
                titulo = "O Exterminador 3",
                categoria = "FA"
            };

            dc.Filmes.InsertOnSubmit(f); // grava na tabela do Linq e não na base de dados

            // para gravar na base de dados
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var lista = from Filme in dc.Filmes select Filme;

            foreach (Filme filme in lista)
            {
                Console.WriteLine("ID: " + filme.id);
                Console.WriteLine("Titulo: " + filme.titulo);
                Console.WriteLine("Categoria: " + filme.categoria);
                Console.WriteLine();
            }

            Console.WriteLine("Existem {0} filmes", dc.Filmes.Count());

            Console.WriteLine("---------------------- Alterar registo ----------------------");

            //Filme f2 = new Filme();

            int idAlterar = 4;

            var pesquisa = from Filme in dc.Filmes
                where Filme.id == idAlterar
                select Filme;

            foreach (Filme filme in pesquisa)
            {
                Console.WriteLine("ID: " + filme.id);
                Console.WriteLine("Titulo: " + filme.titulo);
                Console.WriteLine("Categoria: " + filme.categoria);
                Console.WriteLine();
            }

            // O objeto "f" que é do tipo Filme, vai ficar a apontar também para o resultado da pesquisa
            // como é só um, vamos buscar logo o primeiro (single)
            f = pesquisa.Single();

            f.titulo = "Este titulo foi alterado";

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            foreach (Filme filme in pesquisa)
            {
                Console.WriteLine("ID: " + filme.id);
                Console.WriteLine("Titulo: " + filme.titulo);
                Console.WriteLine("Categoria: " + filme.categoria);
                Console.WriteLine();
            }

            Console.WriteLine("---------------------- Eliminar registo ----------------------");

            Filme f2 = new Filme();

            var outrapesquisa = from Filme in dc.Filmes
                where Filme.id == 5
                select Filme;

            if (outrapesquisa.Count() == 0)
            {
                Console.WriteLine("O filme a apagar já foi apagado");
                Console.ReadKey();
                return;
            }

            f2 = outrapesquisa.Single(); // buscar registo id == 5

            dc.Filmes.DeleteOnSubmit(f2);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var listaApagada = from Filme in dc.Filmes select Filme;

            Console.ReadKey();
        }
    }
}
