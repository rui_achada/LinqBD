﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD_consola
{
    class Program2
    {
        static void Main3(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();

            // lista de filmes só dramáticos
            //var lista = from Filme in dc.Filmes where Filme.Categoria == "FD" select Filme;

            //Filmes começados por "E"
            //var lista = from Filme in dc.Filmes where Filme.Titulo.StartsWith("E") select Filme;

            //Filmes com conjunto de caracteres no titulo
            var lista = from Filme in dc.Filmes where Filme.titulo.Contains("ext") select Filme;

            foreach (Filme filme in lista)
            {
                Console.WriteLine("ID: " + filme.id);
                Console.WriteLine("Titulo: " + filme.titulo);
                Console.WriteLine("Categoria: " + filme.categoria);
                Console.WriteLine();
            }

            //Agrupar informação - Contar Filmes por categoria

            var novaLista = from Filme in dc.Filmes group Filme by Filme.categoria into c select new { Categoria = c.Key, Contagem = c.Count() };
            // into c --> objeto novo

            foreach (var c in novaLista)
            {
                Console.WriteLine(c.Categoria + " (" + c.Contagem + ")");
            }

            Console.WriteLine("----------------- Junção entre tabelas --------------------------");

            //---------------Junção entre tabelas --------------

                var outraLista = from Filme in dc.Filmes
                join Categoria in dc.Categorias on Filme.categoria equals Categoria.Sigla
                select new
                {
                    Filme.id,
                    Filme.titulo,

                    Categoria.Categoria1
                };

            foreach (var c in outraLista)
            {
                Console.WriteLine("ID: " + c.id);
                Console.WriteLine("Titulo: " + c.titulo);
                Console.WriteLine("Categoria: " + c.Categoria1);
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
