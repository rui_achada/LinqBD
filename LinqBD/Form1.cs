﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LinqBD_consola;

namespace LinqBD
{
    public partial class Form1 : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Listview
            listView1.Columns.Add("ID");
            listView1.Columns.Add("Titulo");
            listView1.Columns.Add("Categoria");

            // Carregar os filmes via Linq
            var lista = from Filme in dc.Filmes select Filme;

            foreach (Filme filme in lista)
            {
                ListViewItem item; // um registo
                item = listView1.Items.Add(filme.id.ToString()); // o item vai ser o ID
                item.SubItems.Add(filme.titulo); // os sub items vão ser o titulo e categoria
                item.SubItems.Add(filme.categoria);
            }

            for (int i = 0; i <= 2; i++)
            {
                listView1.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize); // ajustar tamanho
            }

            // TreeView

            var outralista = from Categoria in dc.Categorias select Categoria;

            foreach (Categoria categoria in outralista)
            {
                treeView1.Nodes.Add(categoria.Sigla);
            }

            // Segundo nível da TreeView - filmes

            var listaFilmes = from Filme in dc.Filmes
                orderby Filme.titulo
                select Filme;

            string catFilme;

            foreach (Filme filme in listaFilmes)
            {
                catFilme = filme.categoria;

                foreach (TreeNode node in treeView1.Nodes)
                {
                    if (node.Text == catFilme)
                    {
                        node.Nodes.Add(filme.titulo);
                    }
                }
            }

            // GridView

            dataGridView1.Columns.Add("colId","ID");
            dataGridView1.Columns.Add("colTitulo", "Titulo");
            dataGridView1.Columns.Add("colCategoria", "Categoria");

            int linha = 0;

            DataGridViewCellStyle estilo = new DataGridViewCellStyle();
            estilo.ForeColor = Color.Red;

            foreach (Filme filme in lista)
            {
                DataGridViewRow registo = new DataGridViewRow();
                dataGridView1.Rows.Add(registo); // Cria linhas no datagrid sem dados

                dataGridView1.Rows[linha].Cells[0].Value = filme.id;
                dataGridView1.Rows[linha].Cells[1].Value = filme.titulo;
                dataGridView1.Rows[linha].Cells[2].Value = filme.categoria;

                if ((string) dataGridView1.Rows[linha].Cells[2].Value == "FA")
                {
                    dataGridView1.Rows[linha].DefaultCellStyle = estilo;
                }

                linha++;
            }

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells; // ajudar tamanho

        }
    }
}
